#!/bin/sh 

#  createAsMacFramework.sh
#  part of Graphviz
#
#  Created by Klaus Kneupner on 31/12/2020.

GVROOT="."
GVTARGET="${GVROOT}/_build"
#PLATFORM_DEVELOPER_BIN_DIR="/usr/bin"
export GV_FRAMEWORK_DIR="${GVROOT}/frameworks"
GVCONFIG="Debug"

CC="/Library/Developer/CommandLineTools/usr/bin/clang"
 #  CPP="${PLATFORM_DEVELOPER_BIN_DIR}/clang -E"
CXX="/Library/Developer/CommandLineTools/usr/bin/clang++"
#   OBJC="${PLATFORM_DEVELOPER_BIN_DIR}/clang"
#   LD="${PLATFORM_DEVELOPER_BIN_DIR}/ld"

cmake \
    -G Xcode \
	-H${GVROOT} \
	-B${GVTARGET} \
	"-DCMAKE_C_COMPILER=${CC}" \
	"-DCMAKE_CXX_COMPILER=${CXX}" \
    "-DGV_FRAMEWORK_DIR=${GV_FRAMEWORK_DIR}" \
    "-DLTDL_LIBRARY=~/xcode/SwiftGraphviz/universal/libltdlc.a" \
    "-DGD_LIBRARY=~/xcode/SwiftGraphviz/universal/libgd.a" \
    -UENABLE_LTDL \
    -DENABLE_LTDL=ON \
    -UBUILD_SHARED_LIBS \
    -DBUILD_SHARED_LIBS=OFF \
    -Uenable_macos_universal \
    -Denable_macos_universal=ON


#doesn't work. not sure why
xcodebuild -target package -project ${GVTARGET}/Graphviz.xcodeproj/ -configuration ${GVCONFIG}
#-arch x86_64 -arch arm64-DONLY_ACTIVE_ARCH=NO
